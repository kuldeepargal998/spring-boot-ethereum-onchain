package com.pocketpay.code;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.web3j.abi.FunctionEncoder;
import org.web3j.abi.datatypes.Function;
import org.web3j.abi.datatypes.Utf8String;
import org.web3j.crypto.Bip32ECKeyPair;
import org.web3j.crypto.CipherException;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.MnemonicUtils;
import org.web3j.crypto.RawTransaction;
import org.web3j.crypto.WalletUtils;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.RemoteCall;
import org.web3j.protocol.core.methods.response.EthGasPrice;
import org.web3j.protocol.core.methods.response.EthGetBalance;
import org.web3j.protocol.core.methods.response.EthGetTransactionCount;
import org.web3j.protocol.core.methods.response.Web3ClientVersion;
import org.web3j.protocol.http.HttpService;
import org.web3j.tx.TransactionManager;
import org.web3j.tx.gas.DefaultGasProvider;
import org.web3j.utils.Convert;
import org.web3j.utils.Convert.Unit;
import java.util.Optional;
import org.web3j.crypto.TransactionEncoder;
import org.web3j.protocol.core.methods.response.EthGetTransactionReceipt;
import org.web3j.protocol.core.methods.response.EthSendTransaction;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.utils.Numeric;

public class Practice {

	public static void main(String[] args) {
		try {

			System.out.println("Try to connect network");
			Web3j web3j = Web3j.build(new HttpService("https://data-seed-prebsc-1-s1.binance.org:8545"));
			System.out.println("Network Connect");

			// Example 1
			// System.out.println("Version :- " + Practice.getVersion(web3j));
			// Example 2
			// System.out.println("Default gas price :-" +
			// Practice.getDefaultGasPrice(web3j));
			// Example 3

			System.out.println("Wallet Balance :-"
					+ Practice.getWalletBalance(web3j, "0xb4ac936e51e80425f786e418a77ecb14dafced71"));

			// Example 4
			/*
			 * System.out.println("Nonce (Total transaction of wallet):-" +
			 * Practice.getNonce(web3j, "0x33fbfEA30c6d70b468daa48220DcF920404DC4eA"));
			 */
			// Example 5
			/*
			 * System.out.println("Wallet Information:-" + Practice
			 * .getWalletAndKey("7389810820",
			 * "robust special one soap bicycle mansion afraid spawn grief acquire robot valve"
			 * ) .toString());
			 */

			// Example 6
			// Practice.createWallet();

			// Example 7
			// Practice.loadWallet();

			// Example 8 (Every time private key change)
			// Practice.getWalletByPrivateKey("b8ee176d6073ac48e63fd732f4100555bb4cbbcbf0a0bc98f62c8a3cc9370f3a");

			// Example 9
			// Practice.sendFund(web3j);

			// Example 10
			// compile smart contract and generate abi and bin file
			// command is :- solc DocumentRegistry.sol --bin --abi --optimize -o ./ (using
			// cmd)

			// Example 11
			Practice.transferToken(web3j, "0xb4Ac936E51e80425F786E418a77ECB14DaFcEd71");

		} catch (Exception e) {
			System.out.println("-----Excecption----");
			e.printStackTrace();
		}
	}

	public static void transferToken(Web3j web3j, String receiptAddress) throws Exception {
		// 0x98F02A7CF0Ceebc0daE52E67e87c662C145EB20B
		Credentials creds = Credentials.create("");
		ERC20Basic documentRegistry1 = ERC20Basic.load("0x5818209Fb829311B438431cB1111dA7a3d9B04FB", web3j, creds,
				new DefaultGasProvider());
		BigInteger tokenNo = new BigInteger("100000000000000000000");
		TransactionReceipt receipt = documentRegistry1.transfer(receiptAddress, tokenNo).send();
		String txHash = receipt.getTransactionHash();
		System.out.print(txHash);
	}

	public static void interactWithSmartContract(Web3j web3j) throws Exception {
		Credentials creds = Credentials.create("");
		DocumentRegistry documentRegistry = DocumentRegistry.load("0x83a30b0029eb86975d43deb75ee1daf7e51376f8", web3j,
				creds, new DefaultGasProvider());
		TransactionReceipt receipt = documentRegistry.store("Ritika").send();
		String txHash = receipt.getTransactionHash();
		System.out.print(txHash);
	}

	public static void retrieveWithSmartContract(Web3j web3j) throws Exception {
		Credentials creds = Credentials.create("");
		DocumentRegistry documentRegistry = DocumentRegistry.load("0x83a30b0029eb86975d43deb75ee1daf7e51376f8", web3j,
				creds, new DefaultGasProvider());
		TransactionReceipt receipt = documentRegistry.retrieve().send();
		String txHash = receipt.getTransactionHash();
		System.out.print(txHash);
	}

	public static void contractDeploy(Web3j web3) throws Exception {
		// Create credentials from private key
		Credentials creds = Credentials.create("");

		DocumentRegistry registryContract = DocumentRegistry.deploy(web3, creds, new DefaultGasProvider()).send();

		String contractAddress = registryContract.getContractAddress();
		System.out.print(contractAddress);

		// 0x83a30b0029eb86975d43deb75ee1daf7e51376f8
	}

	/*
	 * public static void connectWithSmartContractSave(Web3j web3) throws Exception
	 * { Credentials creds = Credentials.create(
	 * "");
	 * 
	 * DocumentRegistry registryContract = DocumentRegistry.load(creds.getAddress(),
	 * web3, creds, new DefaultGasProvider());
	 * 
	 * System.out.println("contract address: " +
	 * registryContract.getContractAddress());
	 * 
	 * }
	 */

	public static void sendFund(Web3j web3) throws IOException, CipherException, InterruptedException {
		String walletPassword = "secr3t";
		String walletPath = "F:/WalletDestination/UTC--2021-11-30T06-37-05.633416500Z--b4ac936e51e80425f786e418a77ecb14dafced71.json";

		// Decrypt and open the wallet into a Credential object
		Credentials credentials = WalletUtils.loadCredentials(walletPassword, walletPath);

		// Get nonce
		EthGetTransactionCount ethGetTransactionCount = web3
				.ethGetTransactionCount(credentials.getAddress(), DefaultBlockParameterName.LATEST).send();
		BigInteger nonce = ethGetTransactionCount.getTransactionCount();

		// Recipient account
		String recipientAddress = "0x33fbfEA30c6d70b468daa48220DcF920404DC4eA";

		// Value to Transfer
		BigInteger value = Convert.toWei("1", Unit.ETHER).toBigInteger();

		BigInteger gasLimit = BigInteger.valueOf(21000);
		BigInteger gasPrice = Convert.toWei("1", Unit.GWEI).toBigInteger();

		// Prepare the rawTransaction
		RawTransaction rawTransaction = RawTransaction.createEtherTransaction(nonce, gasPrice, gasLimit,
				recipientAddress, value);

		// Sign the transaction
		byte[] signedMessage = TransactionEncoder.signMessage(rawTransaction, credentials);
		String hexValue = Numeric.toHexString(signedMessage);

		// Send transaction
		EthSendTransaction ethSendTransaction = web3.ethSendRawTransaction(hexValue).send();
		String transactionHash = ethSendTransaction.getTransactionHash();
		System.out.println("transactionHash: " + transactionHash);

		// Wait for transaction to be mined
		Optional<TransactionReceipt> transactionReceipt = null;
		do {
			System.out.println("checking if transaction " + transactionHash + " is mined....");
			EthGetTransactionReceipt ethGetTransactionReceiptResp = web3.ethGetTransactionReceipt(transactionHash)
					.send();
			transactionReceipt = ethGetTransactionReceiptResp.getTransactionReceipt();
			Thread.sleep(3000); // Wait 3 sec
		} while (!transactionReceipt.isPresent());

		System.out.println("Transaction " + transactionHash + " was mined in block # "
				+ transactionReceipt.get().getBlockNumber());
		System.out.println("Balance: "
				+ Convert.fromWei(web3.ethGetBalance(credentials.getAddress(), DefaultBlockParameterName.LATEST).send()
						.getBalance().toString(), Unit.ETHER));

	}

	public static void getWalletByPrivateKey(String privateKey) {
		String pk = privateKey;

		Credentials credentials = Credentials.create(pk);
		System.out.println("Address is " + credentials.getAddress());
	}

	public static void loadWallet() throws IOException, CipherException {
		String walletPassword = "secr3t";
		String walletDirectory = "F:/WalletDestination";
		String walletName = "UTC--2021-11-30T06-37-05.633416500Z--b4ac936e51e80425f786e418a77ecb14dafced71.json";

		// Load the JSON encryted wallet
		Credentials credentials = WalletUtils.loadCredentials(walletPassword, walletDirectory + "/" + walletName);

		// Get the account address
		String accountAddress = credentials.getAddress();

		// Get the unencrypted private key into hexadecimal
		String privateKey = credentials.getEcKeyPair().getPrivateKey().toString(16);

		System.out.println("Private key (match) "
				+ "");
		System.out.println("Address (match) " + "0xb4ac936e51e80425f786e418a77ecb14dafced71");
		System.out.println("Public ket (match) "
				+ "11740124515766781780932716360369841928260573105837674829505791390438546962389584856685303309072194882275359592156963613605952398924686471452592670020083357");
		System.out.println("---------------------------------------");
		System.out.println("Private ket " + privateKey);
		System.out.println("Address is " + accountAddress);
	}

	public static void createWallet() throws InvalidAlgorithmParameterException, NoSuchAlgorithmException,
			NoSuchProviderException, CipherException, IOException {
		String walletPassword = "secr3t";
		String walletDirectory = "F:/WalletDestination";

		String walletName = WalletUtils.generateNewWalletFile(walletPassword, new File(walletDirectory));
		System.out.println("wallet location: " + walletDirectory + "/" + walletName);

		Credentials credentials = WalletUtils.loadCredentials(walletPassword, walletDirectory + "/" + walletName);

		String accountAddress = credentials.getAddress();
		System.out.println("Account address: " + accountAddress);
		System.out.println("Account address: " + credentials.getAddress());
		System.out.println("Private key: " + credentials.getEcKeyPair().getPrivateKey());
		System.out.println("Public key: " + credentials.getEcKeyPair().getPublicKey());

	}

	public static Map<String, Object> getWalletAndKey(String _password, String _mnemonic) {
		Map<String, Object> res = new HashMap<String, Object>();
		String password = _password;
		String mnemonic = _mnemonic;
		// Derivation path wanted: // m/44'/60'/0'/0
		int[] derivationPath = { 44 | Bip32ECKeyPair.HARDENED_BIT, 60 | Bip32ECKeyPair.HARDENED_BIT,
				0 | Bip32ECKeyPair.HARDENED_BIT, 0, 0 };

		// Generate a BIP32 master keypair from the mnemonic phrase
		Bip32ECKeyPair masterKeypair = Bip32ECKeyPair.generateKeyPair(MnemonicUtils.generateSeed(mnemonic, password));

		// Derived the key using the derivation path
		Bip32ECKeyPair derivedKeyPair = Bip32ECKeyPair.deriveKeyPair(masterKeypair, derivationPath);

		// Load the wallet for the derived key
		Credentials credentials = Credentials.create(derivedKeyPair);
		res.put("Wallet is ", credentials.getAddress());
		res.put("Private Key", credentials.getEcKeyPair().getPrivateKey());
		res.put("Public Key", credentials.getEcKeyPair().getPublicKey());

		// Credentials credentials1 =
		// Credentials.create(credentials.getEcKeyPair().getPrivateKey().toString());
		// res.put("By private key (Address)", credentials1.getAddress());
		// res.put("By private key (Private key)",
		// credentials1.getEcKeyPair().getPublicKey().toString());
		// res.put("By private key (Public key)",
		// credentials1.getEcKeyPair().getPrivateKey().toString());
		// by private key
		return res;
	}

	public static Object getNonce(Web3j web3, String walletAddress) throws IOException {

		EthGetTransactionCount ethGetTransactionCount = web3
				.ethGetTransactionCount(walletAddress, DefaultBlockParameterName.LATEST).send();

		BigInteger nonce = ethGetTransactionCount.getTransactionCount();
		return nonce;
	}

	public static Object getWalletBalance(Web3j web3j, String accountAddress) throws IOException {

		EthGetBalance balanceWei = web3j.ethGetBalance(accountAddress, DefaultBlockParameterName.LATEST).send();
		System.out.println("balance in wei: " + balanceWei);

		BigDecimal balanceInEther = Convert.fromWei(balanceWei.getBalance().toString(), Unit.ETHER);
		System.out.println("balance in ether: " + balanceInEther);
		return balanceInEther;
	}

	public static Object getDefaultGasPrice(Web3j web3j) throws IOException {
		EthGasPrice gas = web3j.ethGasPrice().send();
		return gas.getGasPrice();
	}

	public static String getVersion(Web3j web3j) throws IOException {
		Web3ClientVersion version = web3j.web3ClientVersion().send();
		return version.getWeb3ClientVersion();
	}

}
