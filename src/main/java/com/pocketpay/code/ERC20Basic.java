package com.pocketpay.code;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import org.web3j.abi.EventEncoder;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Address;
import org.web3j.abi.datatypes.Event;
import org.web3j.abi.datatypes.Function;
import org.web3j.abi.datatypes.Type;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameter;
import org.web3j.protocol.core.RemoteCall;
import org.web3j.protocol.core.methods.request.EthFilter;
import org.web3j.protocol.core.methods.response.Log;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.tx.Contract;
import org.web3j.tx.TransactionManager;
import org.web3j.tx.gas.ContractGasProvider;
import rx.Observable;
import rx.functions.Func1;

/**
 * <p>
 * Auto generated code.
 * <p>
 * <strong>Do not modify!</strong>
 * <p>
 * Please use the <a href="https://docs.web3j.io/command_line.html">web3j
 * command line tools</a>, or the
 * org.web3j.codegen.SolidityFunctionWrapperGenerator in the
 * <a href="https://github.com/web3j/web3j/tree/master/codegen">codegen
 * module</a> to update.
 *
 * <p>
 * Generated with web3j version 3.6.0.
 */
public class ERC20Basic extends Contract {
	private static final String BINARY = "0x608060405269152d02c7e14af680000060025534801561001e57600080fd5b506002546000803373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002081905550610dfb806100736000396000f3fe608060405234801561001057600080fd5b50600436106100935760003560e01c8063313ce56711610066578063313ce5671461013457806370a082311461015257806395d89b4114610182578063a9059cbb146101a0578063dd62ed3e146101d057610093565b806306fdde0314610098578063095ea7b3146100b657806318160ddd146100e657806323b872dd14610104575b600080fd5b6100a0610200565b6040516100ad9190610a8b565b60405180910390f35b6100d060048036038101906100cb9190610b46565b610239565b6040516100dd9190610ba1565b60405180910390f35b6100ee61032b565b6040516100fb9190610bcb565b60405180910390f35b61011e60048036038101906101199190610be6565b610335565b60405161012b9190610ba1565b60405180910390f35b61013c6106b0565b6040516101499190610c55565b60405180910390f35b61016c60048036038101906101679190610c70565b6106b5565b6040516101799190610bcb565b60405180910390f35b61018a6106fd565b6040516101979190610a8b565b60405180910390f35b6101ba60048036038101906101b59190610b46565b610736565b6040516101c79190610ba1565b60405180910390f35b6101ea60048036038101906101e59190610c9d565b610918565b6040516101f79190610bcb565b60405180910390f35b6040518060400160405280600d81526020017f5570736f6369616c42617369630000000000000000000000000000000000000081525081565b600081600160003373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060008573ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001908152602001600020819055508273ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff167f8c5be1e5ebec7d5bd14f71427d1e84f3dd0314c0f7b2291e5b200ac8c7c3b925846040516103199190610bcb565b60405180910390a36001905092915050565b6000600254905090565b60008060008573ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020019081526020016000205482111561038257600080fd5b600160008573ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060003373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020019081526020016000205482111561040b57600080fd5b61045c826000808773ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020019081526020016000205461099f90919063ffffffff16565b6000808673ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020019081526020016000208190555061052d82600160008773ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060003373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020019081526020016000205461099f90919063ffffffff16565b600160008673ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060003373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001908152602001600020819055506105fe826000808673ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001908152602001600020546109c690919063ffffffff16565b6000808573ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001908152602001600020819055508273ffffffffffffffffffffffffffffffffffffffff168473ffffffffffffffffffffffffffffffffffffffff167fddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef8460405161069d9190610bcb565b60405180910390a3600190509392505050565b601281565b60008060008373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001908152602001600020549050919050565b6040518060400160405280600481526020017f555053540000000000000000000000000000000000000000000000000000000081525081565b60008060003373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020019081526020016000205482111561078357600080fd5b6107d4826000803373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020019081526020016000205461099f90919063ffffffff16565b6000803373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002081905550610867826000808673ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001908152602001600020546109c690919063ffffffff16565b6000808573ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001908152602001600020819055508273ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff167fddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef846040516109069190610bcb565b60405180910390a36001905092915050565b6000600160008473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060008373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002054905092915050565b6000828211156109b2576109b1610cdd565b5b81836109be9190610d3b565b905092915050565b60008082846109d59190610d6f565b9050838110156109e8576109e7610cdd565b5b8091505092915050565b600081519050919050565b600082825260208201905092915050565b60005b83811015610a2c578082015181840152602081019050610a11565b83811115610a3b576000848401525b50505050565b6000601f19601f8301169050919050565b6000610a5d826109f2565b610a6781856109fd565b9350610a77818560208601610a0e565b610a8081610a41565b840191505092915050565b60006020820190508181036000830152610aa58184610a52565b905092915050565b600080fd5b600073ffffffffffffffffffffffffffffffffffffffff82169050919050565b6000610add82610ab2565b9050919050565b610aed81610ad2565b8114610af857600080fd5b50565b600081359050610b0a81610ae4565b92915050565b6000819050919050565b610b2381610b10565b8114610b2e57600080fd5b50565b600081359050610b4081610b1a565b92915050565b60008060408385031215610b5d57610b5c610aad565b5b6000610b6b85828601610afb565b9250506020610b7c85828601610b31565b9150509250929050565b60008115159050919050565b610b9b81610b86565b82525050565b6000602082019050610bb66000830184610b92565b92915050565b610bc581610b10565b82525050565b6000602082019050610be06000830184610bbc565b92915050565b600080600060608486031215610bff57610bfe610aad565b5b6000610c0d86828701610afb565b9350506020610c1e86828701610afb565b9250506040610c2f86828701610b31565b9150509250925092565b600060ff82169050919050565b610c4f81610c39565b82525050565b6000602082019050610c6a6000830184610c46565b92915050565b600060208284031215610c8657610c85610aad565b5b6000610c9484828501610afb565b91505092915050565b60008060408385031215610cb457610cb3610aad565b5b6000610cc285828601610afb565b9250506020610cd385828601610afb565b9150509250929050565b7f4e487b7100000000000000000000000000000000000000000000000000000000600052600160045260246000fd5b7f4e487b7100000000000000000000000000000000000000000000000000000000600052601160045260246000fd5b6000610d4682610b10565b9150610d5183610b10565b925082821015610d6457610d63610d0c565b5b828203905092915050565b6000610d7a82610b10565b9150610d8583610b10565b9250827fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff03821115610dba57610db9610d0c565b5b82820190509291505056fea26469706673582212202d5f8f0e5ecad0130dec697922447e450fc7cb75e823659e361f401e0a92e8c864736f6c634300080a0033";

	public static final String FUNC_DECIMALS = "decimals";

	public static final String FUNC_NAME = "name";

	public static final String FUNC_SYMBOL = "symbol";

	public static final String FUNC_TOTALSUPPLY = "totalSupply";

	public static final String FUNC_BALANCEOF = "balanceOf";

	public static final String FUNC_TRANSFER = "transfer";

	public static final String FUNC_APPROVE = "approve";

	public static final String FUNC_ALLOWANCE = "allowance";

	public static final String FUNC_TRANSFERFROM = "transferFrom";

	public static final Event APPROVAL_EVENT = new Event("Approval",
			Arrays.<TypeReference<?>>asList(new TypeReference<Address>(true) {
			}, new TypeReference<Address>(true) {
			}, new TypeReference<Uint256>() {
			}));;

	public static final Event TRANSFER_EVENT = new Event("Transfer",
			Arrays.<TypeReference<?>>asList(new TypeReference<Address>(true) {
			}, new TypeReference<Address>(true) {
			}, new TypeReference<Uint256>() {
			}));;

	protected static final HashMap<String, String> _addresses;

	static {
		_addresses = new HashMap<String, String>();
	}

	@Deprecated
	protected ERC20Basic(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice,
			BigInteger gasLimit) {
		super(BINARY, contractAddress, web3j, credentials, gasPrice, gasLimit);
	}

	protected ERC20Basic(String contractAddress, Web3j web3j, Credentials credentials,
			ContractGasProvider contractGasProvider) {
		super(BINARY, contractAddress, web3j, credentials, contractGasProvider);
	}

	@Deprecated
	protected ERC20Basic(String contractAddress, Web3j web3j, TransactionManager transactionManager,
			BigInteger gasPrice, BigInteger gasLimit) {
		super(BINARY, contractAddress, web3j, transactionManager, gasPrice, gasLimit);
	}

	protected ERC20Basic(String contractAddress, Web3j web3j, TransactionManager transactionManager,
			ContractGasProvider contractGasProvider) {
		super(BINARY, contractAddress, web3j, transactionManager, contractGasProvider);
	}

	public static RemoteCall<ERC20Basic> deploy(Web3j web3j, Credentials credentials,
			ContractGasProvider contractGasProvider) {
		return deployRemoteCall(ERC20Basic.class, web3j, credentials, contractGasProvider, BINARY, "");
	}

	public static RemoteCall<ERC20Basic> deploy(Web3j web3j, TransactionManager transactionManager,
			ContractGasProvider contractGasProvider) {
		return deployRemoteCall(ERC20Basic.class, web3j, transactionManager, contractGasProvider, BINARY, "");
	}

	@Deprecated
	public static RemoteCall<ERC20Basic> deploy(Web3j web3j, Credentials credentials, BigInteger gasPrice,
			BigInteger gasLimit) {
		return deployRemoteCall(ERC20Basic.class, web3j, credentials, gasPrice, gasLimit, BINARY, "");
	}

	@Deprecated
	public static RemoteCall<ERC20Basic> deploy(Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice,
			BigInteger gasLimit) {
		return deployRemoteCall(ERC20Basic.class, web3j, transactionManager, gasPrice, gasLimit, BINARY, "");
	}

	public List<ApprovalEventResponse> getApprovalEvents(TransactionReceipt transactionReceipt) {
		List<Contract.EventValuesWithLog> valueList = extractEventParametersWithLog(APPROVAL_EVENT, transactionReceipt);
		ArrayList<ApprovalEventResponse> responses = new ArrayList<ApprovalEventResponse>(valueList.size());
		for (Contract.EventValuesWithLog eventValues : valueList) {
			ApprovalEventResponse typedResponse = new ApprovalEventResponse();
			typedResponse.log = eventValues.getLog();
			typedResponse.owner = (String) eventValues.getIndexedValues().get(0).getValue();
			typedResponse.spender = (String) eventValues.getIndexedValues().get(1).getValue();
			typedResponse.value = (BigInteger) eventValues.getNonIndexedValues().get(0).getValue();
			responses.add(typedResponse);
		}
		return responses;
	}

	/*
	 * public Observable<ApprovalEventResponse> approvalEventObservable(EthFilter
	 * filter) { return web3j.ethLogObservable(filter).map(new Func1<Log,
	 * ApprovalEventResponse>() {
	 * 
	 * @Override public ApprovalEventResponse call(Log log) {
	 * Contract.EventValuesWithLog eventValues =
	 * extractEventParametersWithLog(APPROVAL_EVENT, log); ApprovalEventResponse
	 * typedResponse = new ApprovalEventResponse(); typedResponse.log = log;
	 * typedResponse.owner = (String)
	 * eventValues.getIndexedValues().get(0).getValue(); typedResponse.spender =
	 * (String) eventValues.getIndexedValues().get(1).getValue();
	 * typedResponse.value = (BigInteger)
	 * eventValues.getNonIndexedValues().get(0).getValue(); return typedResponse; }
	 * }); }
	 */

	/*
	 * public Observable<ApprovalEventResponse>
	 * approvalEventObservable(DefaultBlockParameter startBlock,
	 * DefaultBlockParameter endBlock) { EthFilter filter = new
	 * EthFilter(startBlock, endBlock, getContractAddress());
	 * filter.addSingleTopic(EventEncoder.encode(APPROVAL_EVENT)); return
	 * approvalEventObservable(filter); }
	 */

	public List<TransferEventResponse> getTransferEvents(TransactionReceipt transactionReceipt) {
		List<Contract.EventValuesWithLog> valueList = extractEventParametersWithLog(TRANSFER_EVENT, transactionReceipt);
		ArrayList<TransferEventResponse> responses = new ArrayList<TransferEventResponse>(valueList.size());
		for (Contract.EventValuesWithLog eventValues : valueList) {
			TransferEventResponse typedResponse = new TransferEventResponse();
			typedResponse.log = eventValues.getLog();
			typedResponse.from = (String) eventValues.getIndexedValues().get(0).getValue();
			typedResponse.to = (String) eventValues.getIndexedValues().get(1).getValue();
			typedResponse.value = (BigInteger) eventValues.getNonIndexedValues().get(0).getValue();
			responses.add(typedResponse);
		}
		return responses;
	}

	/*
	 * public Observable<TransferEventResponse> transferEventObservable(EthFilter
	 * filter) { return web3j.ethLogObservable(filter).map(new Func1<Log,
	 * TransferEventResponse>() {
	 * 
	 * @Override public TransferEventResponse call(Log log) {
	 * Contract.EventValuesWithLog eventValues =
	 * extractEventParametersWithLog(TRANSFER_EVENT, log); TransferEventResponse
	 * typedResponse = new TransferEventResponse(); typedResponse.log = log;
	 * typedResponse.from = (String)
	 * eventValues.getIndexedValues().get(0).getValue(); typedResponse.to = (String)
	 * eventValues.getIndexedValues().get(1).getValue(); typedResponse.value =
	 * (BigInteger) eventValues.getNonIndexedValues().get(0).getValue(); return
	 * typedResponse; } }); }
	 */

	/*
	 * public Observable<TransferEventResponse>
	 * transferEventObservable(DefaultBlockParameter startBlock,
	 * DefaultBlockParameter endBlock) { EthFilter filter = new
	 * EthFilter(startBlock, endBlock, getContractAddress());
	 * filter.addSingleTopic(EventEncoder.encode(TRANSFER_EVENT)); return
	 * transferEventObservable(filter); }
	 */

	public RemoteCall<TransactionReceipt> decimals() {
		final Function function = new Function(FUNC_DECIMALS, Arrays.<Type>asList(),
				Collections.<TypeReference<?>>emptyList());
		return executeRemoteCallTransaction(function);
	}

	public RemoteCall<TransactionReceipt> name() {
		final Function function = new Function(FUNC_NAME, Arrays.<Type>asList(),
				Collections.<TypeReference<?>>emptyList());
		return executeRemoteCallTransaction(function);
	}

	public RemoteCall<TransactionReceipt> symbol() {
		final Function function = new Function(FUNC_SYMBOL, Arrays.<Type>asList(),
				Collections.<TypeReference<?>>emptyList());
		return executeRemoteCallTransaction(function);
	}

	public RemoteCall<TransactionReceipt> totalSupply() {
		final Function function = new Function(FUNC_TOTALSUPPLY, Arrays.<Type>asList(),
				Collections.<TypeReference<?>>emptyList());
		return executeRemoteCallTransaction(function);
	}

	public RemoteCall<TransactionReceipt> balanceOf(String tokenOwner) {
		final Function function = new Function(FUNC_BALANCEOF,
				Arrays.<Type>asList(new org.web3j.abi.datatypes.Address(tokenOwner)),
				Collections.<TypeReference<?>>emptyList());
		return executeRemoteCallTransaction(function);
	}

	public RemoteCall<TransactionReceipt> transfer(String receiver, BigInteger numTokens) {
		final Function function = new Function(FUNC_TRANSFER,
				Arrays.<Type>asList(new org.web3j.abi.datatypes.Address(receiver),
						new org.web3j.abi.datatypes.generated.Uint256(numTokens)),
				Collections.<TypeReference<?>>emptyList());
		return executeRemoteCallTransaction(function);
	}

	public RemoteCall<TransactionReceipt> approve(String delegate, BigInteger numTokens) {
		final Function function = new Function(FUNC_APPROVE,
				Arrays.<Type>asList(new org.web3j.abi.datatypes.Address(delegate),
						new org.web3j.abi.datatypes.generated.Uint256(numTokens)),
				Collections.<TypeReference<?>>emptyList());
		return executeRemoteCallTransaction(function);
	}

	public RemoteCall<TransactionReceipt> allowance(String owner, String delegate) {
		final Function function = new Function(FUNC_ALLOWANCE,
				Arrays.<Type>asList(new org.web3j.abi.datatypes.Address(owner),
						new org.web3j.abi.datatypes.Address(delegate)),
				Collections.<TypeReference<?>>emptyList());
		return executeRemoteCallTransaction(function);
	}

	public RemoteCall<TransactionReceipt> transferFrom(String owner, String buyer, BigInteger numTokens) {
		final Function function = new Function(FUNC_TRANSFERFROM,
				Arrays.<Type>asList(new org.web3j.abi.datatypes.Address(owner),
						new org.web3j.abi.datatypes.Address(buyer),
						new org.web3j.abi.datatypes.generated.Uint256(numTokens)),
				Collections.<TypeReference<?>>emptyList());
		return executeRemoteCallTransaction(function);
	}

	@Deprecated
	public static ERC20Basic load(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice,
			BigInteger gasLimit) {
		return new ERC20Basic(contractAddress, web3j, credentials, gasPrice, gasLimit);
	}

	@Deprecated
	public static ERC20Basic load(String contractAddress, Web3j web3j, TransactionManager transactionManager,
			BigInteger gasPrice, BigInteger gasLimit) {
		return new ERC20Basic(contractAddress, web3j, transactionManager, gasPrice, gasLimit);
	}

	public static ERC20Basic load(String contractAddress, Web3j web3j, Credentials credentials,
			ContractGasProvider contractGasProvider) {
		return new ERC20Basic(contractAddress, web3j, credentials, contractGasProvider);
	}

	public static ERC20Basic load(String contractAddress, Web3j web3j, TransactionManager transactionManager,
			ContractGasProvider contractGasProvider) {
		return new ERC20Basic(contractAddress, web3j, transactionManager, contractGasProvider);
	}

	protected String getStaticDeployedAddress(String networkId) {
		return _addresses.get(networkId);
	}

	public static String getPreviouslyDeployedAddress(String networkId) {
		return _addresses.get(networkId);
	}

	public static class ApprovalEventResponse {
		public Log log;

		public String owner;

		public String spender;

		public BigInteger value;
	}

	public static class TransferEventResponse {
		public Log log;

		public String from;

		public String to;

		public BigInteger value;
	}
}
